<?php
    require_once "securimage.php";
    header("Content-Type: application/json");
	$securimage = new Securimage();

	if($securimage->check($_POST['captcha_code'])){
	    echo json_encode(["message"=>0]);
    }else
        echo json_encode(["message"=>1]);
?>